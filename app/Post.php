<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [

    	'title',
    	'content'

    ];

    public function comments()
    {	
    	#Quer dizer que, em um post.. tenho vários comentários.
    	return $this->hasMany('App\Comment');

    }
}
